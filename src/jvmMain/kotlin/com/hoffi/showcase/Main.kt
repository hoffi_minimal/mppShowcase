package com.hoffi.showcase

fun main() {
    println("===================================================================")
    println("===   Hello, Showcase Kotlin/JVM!  (${System.getProperty("os.name")} / ${System.getProperty("os.arch")})   ===")
    println("===================================================================")
}
