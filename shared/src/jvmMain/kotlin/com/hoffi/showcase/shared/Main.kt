package com.hoffi.showcase.shared

fun main() {
    println("==============================================================")
    println("===   Hello, shared Kotlin/JVM!  (${System.getProperty("os.name")} / ${System.getProperty("os.arch")})   ===")
    println("==============================================================")
}
