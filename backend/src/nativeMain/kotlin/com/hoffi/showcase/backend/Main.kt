package com.hoffi.showcase.backend

fun main() {
    println("==================================================")
    println("===   Hello, backend Kotlin/Native! (${Platform.osFamily} / ${Platform.cpuArchitecture})   ===")
    println("==================================================")
}
