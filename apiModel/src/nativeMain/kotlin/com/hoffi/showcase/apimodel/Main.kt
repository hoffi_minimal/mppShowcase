package com.hoffi.showcase.apimodel

fun main() {
    println("==================================================")
    println("===   Hello, apiModel Kotlin/Native! (${Platform.osFamily} / ${Platform.cpuArchitecture})   ===")
    println("==================================================")
}
