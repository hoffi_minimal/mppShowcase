
rootProject.name = "mppShowcase"

include(":apiModel")
include(":backend")
include(":cli")
include(":shared")
