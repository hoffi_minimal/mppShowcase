package com.hoffi.showcase.cli

fun main() {
    println("============================================")
    println("===   Hello, CLI Kotlin/Native! (${Platform.osFamily} / ${Platform.cpuArchitecture})   ===")
    println("============================================")
}
