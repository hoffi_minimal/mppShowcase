package com.hoffi.showcase.shared.json.serializers

import kotlinx.datetime.*
import kotlinx.serialization.KSerializer
import kotlinx.serialization.Serializer
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder

/** lenient ISO-8601<-->UTC LocalDateTime Serializer (e.g. also allowing offset +0100) */
//@Serializer(forClass = LocalDateTime::class)
object LocalDateTimeSerializer : KSerializer<LocalDateTime> {
    //                      1    2   3        4        5        6    7      8      9
    val r = Regex("(.+?)T(([0-9:]+)(\\.\\d+)?([+-Z])(\\d\\d)?(:)?(\\d\\d)?(Z?))$")

    override val descriptor: SerialDescriptor = PrimitiveSerialDescriptor("lenient ISO-8601<-->LocalDateTime Serializer (e.g. also allowing offset +0100)", PrimitiveKind.STRING)

    override fun serialize(encoder: Encoder, value: LocalDateTime) {
        println("${LocalDateTimeSerializer::class.simpleName}.serialize(${value})")
        encoder.encodeString(value.toInstant(TimeZone.currentSystemDefault()).toString()) // to UTC (Zulu) time
    }

    override fun deserialize(decoder: Decoder): LocalDateTime {
        val string = decoder.decodeString()
        println("${LocalDateTimeSerializer::class.simpleName}.deserialize(\"$string\")")
        val matches = r.find(string)
        val lenient = if (matches?.groupValues?.get(5) == "Z") {
            string // zulu time without offset
        } else {
            "${matches?.groupValues?.get(1)}T${matches?.groupValues?.get(3)}${matches?.groupValues?.get(4)}${matches?.groupValues?.get(5)}${matches?.groupValues?.get(6)}" +
                    if (matches?.groupValues?.get(7)?.isEmpty() == true) ":" else ":" +
                            "${matches?.groupValues?.get(8)}${matches?.groupValues?.get(9)}"
        }
        return Instant.parse(lenient).toLocalDateTime(TimeZone.UTC)
    }
}
