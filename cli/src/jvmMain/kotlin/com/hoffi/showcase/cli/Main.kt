package com.hoffi.showcase.cli

fun main() {
    println("========================================================")
    println("===   Hello, CLI Kotlin/JVM!  (${System.getProperty("os.name")} / ${System.getProperty("os.arch")})   ===")
    println("========================================================")
}
