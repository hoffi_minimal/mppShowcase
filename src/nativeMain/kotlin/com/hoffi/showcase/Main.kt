package com.hoffi.showcase

fun main() {
    println("==================================================")
    println("===   Hello, Kotlin/Native! (${Platform.osFamily} / ${Platform.cpuArchitecture})   ===")
    println("==================================================")
}
