# mppShowcase README.md

## Project Layout

tbd

### Intellij idea scope coloring

for better orientation while code browsing you can use intellij scopes to color certain files/folders/modules.

e.g. define scopes (in Settings... `Scopes`):

- scope `00__` (scope with all folders where the name starts with: `0[0-3]__`, meaning the first folder
  - `file:01__*/||file:02__*/||file:03__*/||file:ZZ__*/||file:ZZ__*`
- scope `src`
  - `file:_src*/` (scope with all folders where the name starts with `_src`)
- scope `buildfiles`
  - `file:build.gradle.kts||file:gradle.properties||file:settings.gradle.kts`

and then in Settings... `File Colors` add the scope(s) and give them a color.

If you _then_ add folders/files matching the above scope names
you can see more clearly which "area" of code in the folder structure you are just looking at the moment.
