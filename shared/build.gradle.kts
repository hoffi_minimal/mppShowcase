plugins {
    kotlin("multiplatform")
    kotlin("plugin.serialization")
    id("com.github.johnrengelman.shadow")
    application
}

val jdkVersion: Int by rootProject.extra

group = "${rootProject.group}"
version = "${rootProject.version}"
val artifactName: String by extra { "${rootProject.name.toLowerCase()}-${project.name.toLowerCase()}" }

val rootPackage: String by rootProject.extra
val projectPackage: String by extra { "${rootPackage}.${project.name.toLowerCase()}" }
val theMainClass: String by extra { "Main" }
application {
    mainClass.set("${projectPackage}.${theMainClass}" + "Kt") // + "Kt" if fun main is outside a class
}

repositories {
    mavenCentral()
}

kotlin {
    jvmToolchain(jdkVersion)
    val nativeTarget = when(BuildSrcGlobal.hostOS) {
        BuildSrcGlobal.HOSTOS.MACOS -> macosX64("native")
        BuildSrcGlobal.HOSTOS.LINUX -> linuxX64("native")
        BuildSrcGlobal.HOSTOS.WINDOWS -> mingwX64("native")
        else -> throw GradleException("Host OS is not supported in Kotlin/Native.")
    }

    nativeTarget.apply {
        binaries {
            executable {
                entryPoint = "${projectPackage}.${theMainClass.toLowerCase()}"
            }
        }
    }
    jvm {
        jvmToolchain(jdkVersion)
        withJava()
        testRuns["test"].executionTask.configure {
            useJUnitPlatform()
        }
    }
    sourceSets {
        val nativeMain by getting
        val nativeTest by getting
        val jvmMain by getting
        val jvmTest by getting
        val commonMain by getting  { // predefined by gradle multiplatform plugin
            dependencies {
                implementation("org.jetbrains.kotlinx:kotlinx-serialization-json".depAndVersion())
                implementation("org.jetbrains.kotlinx:kotlinx-datetime".depAndVersion())
            }
        }
        val commonTest by getting {
            dependencies {
                implementation(kotlin("test"))
            }
        }
    }
}

tasks {
    shadowJar {
        manifest { attributes["Main-Class"] = theMainClass }
        archiveClassifier.set("fat")
        archiveBaseName.set(artifactName)
//        val jvmJar = named<org.gradle.jvm.tasks.Jar>("jvmJar").get()
//        from(jvmJar.archiveFile)
//        println(">   shadowJar adding fat.jar dependencies:")
//        project.configurations.named("jvmRuntimeClasspath").get().forEachIndexed { i, file ->
//            println("     ${String.format("%2d", i+1)}) ${file.name}")
//        }
//        configurations.add(project.configurations.named("jvmRuntimeClasspath").get())
    }
    jar {
        finalizedBy(shadowJar)
    }
}
